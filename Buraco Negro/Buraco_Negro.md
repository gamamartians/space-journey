# Black Hole
* Region of high gravity that nothing scapes, not even the light;
* As more things fall into a black hole, the bigger it gets;
* It's formed from the compression of large amount of matter in a very small space,
  for example the explosion of a supernova;
* It's impossible to see a black hole, because the light does not come out of it.

# Buraco Negro
* Região de alta gravidade que nada consegue escapar nem mesmo a luz; 
* À medida que mais coisas caem em um buraco negro, maior ele fica;
* É formado a partir da compressão de grande quantidade de matéria em um espaço muito pequeno,
  por exemplo a explosão de uma supernova;
* Não é possível ver um buraco negro, porque a luz não sai dele.

![Buraco Negro](https://www.nasa.gov/sites/default/files/thumbnails/image/pia18919.jpg)