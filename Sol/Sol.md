# Sol
* Estrela central do sistema solar, possui uma massa 332 900 vezes maior que a da Terra;
* Contém cerca de 99,86% da massa do Sistema Solar;
* Temperatura média na superfície: 5700°C;

![sol](https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/20150420_active_regions_171.jpg)