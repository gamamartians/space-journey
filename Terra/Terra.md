# Earth
* The third planet of the solar system;
* The Earth is the only celestial body where the existence of life is known;
* It has a natural satellite = body that rotates around the Earth naturally;
* Diameter: 12 756.2 km = 336 times the Christ Redeemer;
* Mass: 5.9736 × 10 ^ 24 kg = thousands of stages full of whales;
* More than half the surface of the Earth is covered by oceans.

## Process of formation of the Earth thousands of years ago:
```
At first there was a cloud of gas and dust orbiting around the Sun,
but due to the great gravitational attraction of the sun more and more mass was gathering
and so was forming the known planets today.
Before Earth got to what we know she went through a fireball phase
with an average temperature of 1500 ° C and then underwent a cooling where the existing gases
suffered alterations forming the liquid water that favored the existence of life.
```

# Terra
* Terceiro planeta do sistema solar;
* A Terra é o único corpo celeste onde é conhecida a existência de vida;
* Possui um satélite natural = corpo que gira ao redor da Terra naturalmente; 
* Diâmetro: 12 756,2 km = 336 vezes o Cristo Redentor;
* Massa: 5,9736×10^24  kg = milhares de estádios cheios de baleias;
* Mais da metade da superfície da Terra é coberta por oceanos.

## Processo de formação da Terra à milhares de anos atrás: 
```
No início existia uma nuvem de gás e poeira que orbitava ao redor do Sol,
mas devido à grande atração gravitacional do Sol cada vez mais massa foi se juntando
e assim foi formando os planetas conhecidos hoje.  
Antes da Terra chegar ao que conhecemos ela passou por uma fase bola de fogo
com uma temperatura média de 1500°C e depois passou por um resfriamento onde os gases existentes
sofreram alterações formando a água líquida que favoreceu a existência de vida.
```

![Terra](https://www.nasa.gov/sites/default/files/13989104603_c57e9de5cf_o.jpg)